import React from 'react';
import ReactDOM from 'react-dom';
import { 
  View, Panel, PanelHeader, Header, Group, Cell, Div, Text, Button,PanelHeaderBack,CardGrid,Card, 
  FormLayout, Input, Select, Textarea, Link,Radio,FormLayoutGroup,Checkbox, PanelHeaderClose, Tabs, TabsItem,
  PanelHeaderButton, Epic, Tabbar, TabbarItem, InfoRow, Progress
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import '../src/App.css'; 
import Icon28TargetOutline from '@vkontakte/icons/dist/28/target_outline';
import Icon28CalendarOutline from '@vkontakte/icons/dist/28/calendar_outline';
import Icon16Chevron from '@vkontakte/icons/dist/16/chevron';
import Icon28PictureOutline from '@vkontakte/icons/dist/28/picture_outline';
import Icon16Dropdown from '@vkontakte/icons/dist/16/dropdown';
import Icon28ArrowUpOutline from '@vkontakte/icons/dist/28/arrow_up_outline';
import Icon28CameraOutline  from '@vkontakte/icons/dist/28/camera';
import Icon28Notifications from '@vkontakte/icons/dist/28/notifications';
import Icon28ServicesOutline from '@vkontakte/icons/dist/28/services_outline';
import Icon28MessageOutline from '@vkontakte/icons/dist/28/message_outline';
import Icon28UserCircleOutline from '@vkontakte/icons/dist/28/user_circle_outline';
import Icon28NewsfeedOutline from '@vkontakte/icons/dist/28/newsfeed_outline';
import Icon28ClipOutline from '@vkontakte/icons/dist/28/clip_outline';
import cat from '../src/img/cat.png';



class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activePanel: 'main',
      email: '',
      purpose: '',
      activeStory: 'feed'

    }
   

    this.onChange = this.onChange.bind(this);
    this.select = this.select.bind(this);
  }

  select(e) {
    const mode = e.currentTarget.dataset.mode;
    this.setState({ mode, contextOpened: false });
  }

  onChange(e) {
    const { name, value } = e.currentTarget;
    this.setState({ [name]: value });
  }

render(){
  const { email, purpose } = this.state;
  return (
    <View activePanel={this.state.activePanel}>
      <Panel id="main">
        <PanelHeader >Пожертвования</PanelHeader>
        <Div className= "textGrey">
          <span>У вас пока нет сборов.   Начните доброе дело. </span>
          <Button  onClick={ () => this.setState({ activePanel: 'create' }) }>Создать сбор</Button>
        </Div>
      </Panel>
      <Panel id="create">
        <PanelHeader separator={true} left={<PanelHeaderBack onClick={() => this.setState({ activePanel: 'main' })}/>}>Тип сбора</PanelHeader>
        <CardGrid className="collectionTabs">
          <Card size="l" onClick={ () => this.setState({ activePanel: 'targetDonation' }) }> 
            <div style={{ height: 76, padding: 10 }}>
              <Icon28TargetOutline width={35} height={35} style= {{color: "#3f8ae0", float:"left", }}/>
                <Text className="textTabsTitle">Целевой сбор</Text>
                <Text className="textTabs">Когда есть определенная цель</Text>
                <Icon16Chevron  width={35} height={35} style= {{color: "#b8c1cc", float:"right"}}/>
            </div>
          </Card>
          <Card size="l" onClick={ () => this.setState({ activePanel: 'regularDonation' }) }>
            <div style={{ height: 76, padding: 10  }}>
              <Icon28CalendarOutline width={35} height={35} style= {{color: "#3f8ae0", float:"left"}} />
              <Text className="textTabsTitle">Регулярный сбор</Text>
              <Text className="textTabs">Если помощь нужна ежемесячно</Text>
              <Icon16Chevron  width={35} height={35} style= {{color: "#b8c1cc", float:"right"}}/>
            </div>
          </Card>
        </CardGrid>
      </Panel>
      <Panel id="targetDonation">
        <PanelHeader separator={false} left={<PanelHeaderBack onClick={() => this.setState({ activePanel: 'create' })}/>} >Целевой сбор</PanelHeader>
        <CardGrid>
          <Card size="l" className="downloadImg">
            <div style={{ height: 140}}>
              <div className="textDownload">
                <Text style= {{color: "#3f8ae0", display: "inline-block"}}>
                  <Icon28PictureOutline width={35} height={35} style= {{color: "#3f8ae0", display: "inline-block" }}/>
                  Загрузить обложку
                </Text>
              </div>
            </div>
          </Card>
        </CardGrid>
        <FormLayout>
            <Input top="Название сбора" />
            <Input top="Сумма, Р" placeholder="Сколько нужно собрать?"/>
            <Input top="Цель" placeholder="Например, лечение человека"/>
            <Input top="Описание" placeholder="На что пойдут деньги и как они кому-то помогут?"/>
            <Select top="Куда получать деньги" placeholder="Выберете счет">
              <option value="m">VK Pay</option>
              <option value="f">Банковская карта</option>
            </Select>
            <Button size="xl" onClick={ () => this.setState({ activePanel: 'additionally' }) }>Далее</Button>
          </FormLayout>
      </Panel>
      <Panel id="additionally">
        <PanelHeader separator={false} left={<PanelHeaderBack onClick={() => this.setState({ activePanel: 'targetDonation' })}/>} >Дополнительно</PanelHeader>
        <FormLayout>
        <Select top="Автор" placeholder="Выберете автора">
              <option value="k">Матвей</option>
        </Select>
        
        <FormLayoutGroup top="Сбор завершится">
              <Radio name="type">Когда соберем сумму</Radio>
              <Radio name="type">В опреденную дату</Radio>
        </FormLayoutGroup>
        <Select top="Дата окончания" placeholder="Выберете дату">
              <option value="k">Завтра</option>
        </Select>
        <Button size="xl" onClick={ () => this.setState({ activePanel: 'createDonat' }) }>Создать сбор</Button>
        </FormLayout>
      </Panel>
      <Panel id="regularDonation">
        <PanelHeader separator={false} left={<PanelHeaderBack onClick={() => this.setState({ activePanel: 'create' })}/>} >Регулярный сбор</PanelHeader>
        <CardGrid>
          <Card size="l" className="downloadImg">
            <div style={{ height: 140}}>
              <div className="textDownload">
                <Text style= {{color: "#3f8ae0", display: "inline-block"}}>
                  <Icon28PictureOutline width={35} height={35} style= {{color: "#3f8ae0", display: "inline-block" }}/>
                  Загрузить обложку
                </Text>
              </div>
            </div>
          </Card>
        </CardGrid>
        <FormLayout>
            <Input top="Название сбора" />
            <Input top="Сумма, Р" placeholder="Сколько нужно собрать?"/>
            <Input top="Цель" placeholder="Например, лечение человека"/>
            <Input top="Описание" placeholder="На что пойдут деньги и как они кому-то помогут?"/>
            <Select top="Куда получать деньги" placeholder="Выберете счет">
              <option value="m">VK Pay</option>
              <option value="f">Банковская карта</option>
            </Select>
            <Select top="Автор" placeholder="Выберете автора">
              <option value="k">Матвей</option>
            </Select>
            <Button size="xl" onClick={ () => this.setState({ activePanel: 'additionally' }) }>Далее</Button>
          </FormLayout>
      </Panel>
      <Panel id="createDonat">
        <PanelHeader separator={true} 
        left={<PanelHeaderClose  onClick={() => this.setState({ activePanel: 'additionally' })}/>} 
        right={<Icon28ArrowUpOutline  onClick={() => this.setState({ activePanel: 'example' })}/>} >
          Матвей <Icon16Dropdown/>
          </PanelHeader>
          <FormLayout>
            <Textarea  placeholder="Добавьте комментарий" />
          </FormLayout>
      </Panel>
      <Panel id="example">
      <PanelHeader
              left={<PanelHeaderButton><Icon28CameraOutline /></PanelHeaderButton>}
              right={<PanelHeaderButton><Icon28Notifications /></PanelHeaderButton>}
              separator={true}
            >
              <Tabs>
                <TabsItem
                 onClick={() => {
                  if (this.state.activeTab1 === 'news') {
                    this.setState({ contextOpened: !this.state.contextOpened });
                  }
                  this.setState({ activeTab1: 'news' })
                }}
                  selected={this.state.activeTab1 === 'news'}
                  after={<Icon16Dropdown fill="var(--accent)" style={{
                    transform: `rotate(${this.state.contextOpened ? '180deg' : '0'})`
                  }}/>}
                >
                  Новости
                </TabsItem>
                <TabsItem       
                onClick={() => {
                  this.setState({ activeTab1: 'recomendations', contextOpened: false })
                }}          
                  selected={this.state.activeTab1 === 'recomendations'}
                >
                  Интересное
                </TabsItem>
              </Tabs>
            </PanelHeader>
            <Epic activeStory={this.state.activeStory} tabbar={
        <Tabbar>
          <TabbarItem
            onClick={this.onStoryChange}
            selected={this.state.activeStory === 'feed'}
            data-story="feed"
            text="Новости"
          ><Icon28NewsfeedOutline /></TabbarItem>
          <TabbarItem
            onClick={this.onStoryChange}
            selected={this.state.activeStory === 'services'}
            data-story="services"
            text="Сервисы"
          ><Icon28ServicesOutline/></TabbarItem>
          <TabbarItem
            onClick={this.onStoryChange}
            selected={this.state.activeStory === 'messages'}
            data-story="messages"
            label="12"
            text="Сообщения"
          ><Icon28MessageOutline /></TabbarItem>
          <TabbarItem
            onClick={this.onStoryChange}
            selected={this.state.activeStory === 'clips'}
            data-story="clips"
            text="Клипы"
          ><Icon28ClipOutline /></TabbarItem>
          <TabbarItem
            onClick={this.onStoryChange}
            selected={this.state.activeStory === 'profile'}
            data-story="profile"
            text="Профиль"
          ><Icon28UserCircleOutline /></TabbarItem>
        </Tabbar>
      }>      
      </Epic>
      <CardGrid>
          <Card size="l" mode="outline">
            <div style={{ height: 260, borderRadius: 10}}>
            <img style={{ height: 140, borderRadius: 10, width: "100%"}} src={cat}/>
            <div style={{padding: 10, borderBottom: "1px solid #d7d8d9"}} >
              <Text style={{ fontSize:18, fontFamily:"RobotoMedium"}}>Добряши помогают котикам</Text>
              <Text style={{ fontSize:12, color:"#d7d8d9", fontFamily:"RobotoRegular", marginTop:5}}>Матвей Правосудов . Помощь нужна каждый месяц</Text>
            </div>
            <div style={{ padding: 10}}>
              <div style={{ width: 230, display:"inline-block"}}>
                <InfoRow header="Собрано 8750 Р">
                  <Progress value={60} />
                </InfoRow>
              </div>
              <Button mode="outline" style={{display:"inline-block", marginLeft:30}}>Помочь</Button>
            </div>
            </div>
            
          </Card>
        </CardGrid>
      </Panel>
    </View>
  );
}
}
export default App;
